#!/bin/env python3
from base64 import b64encode
from hashlib import sha512

indexcss = open('src/index.css').read()
indexcsssha = sha512()
indexcsssha.update(indexcss.encode('utf8'))
indexcsssha = b64encode(indexcsssha.digest()).decode('utf8')

svgcss = open('src/svg.css').read()
svgcsssha = sha512()
svgcsssha.update(svgcss.encode('utf8'))
svgcsssha = b64encode(svgcsssha.digest()).decode('utf8')

mainjs = open('src/main.js').read()
mainjssha = sha512()
mainjssha.update(mainjs.encode('utf8'))
mainjssha = b64encode(mainjssha.digest()).decode('utf8')

html = open('src/index.html.template').read()
html = html.replace('{{MAIN_JS}}', mainjs, 1)
html = html.replace('{{MAIN_JS_SHA}}', mainjssha, 1)
html = html.replace('{{INDEX_CSS}}', indexcss, 1)
html = html.replace('{{INDEX_CSS_SHA}}', indexcsssha, 1)
html = html.replace('{{SVG_CSS}}', svgcss, 1)
html = html.replace('{{SVG_CSS_SHA}}', svgcsssha, 1)

output = open('./index.html', "w")
output.write(html)
output.close()
