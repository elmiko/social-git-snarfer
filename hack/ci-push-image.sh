#!/bin/sh
set -x

if [ $CI_COMMIT_REF_NAME == "master" ]
then
docker login -u elmiko+snarferbot -p $SGS_REGISTRY_TOKEN $SGS_TARGET_REGISTRY
docker tag $SGS_BUILD_CONTAINER:$CI_COMMIT_REF_NAME $SGS_TARGET_REGISTRY:latest
docker push $SGS_TARGET_REGISTRY:latest
fi
