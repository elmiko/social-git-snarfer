# social-git-snarfer

[![Docker Repository on Quay](https://quay.io/repository/elmiko/social-git-snarfer/status "Docker Repository on Quay")](https://quay.io/repository/elmiko/social-git-snarfer)

an experiment with gitlab and github event streams

this project builds an html file which provides access to viewing the aggregated
public event streams for users across github and gitlab. the file is completely
self contained, but must be produced with the provided build scripts.

## build index.html

```bash
make clean all
```

then open `index.html` in the browser of your choice, all instructions for usage
are contained within the file.

## make a release

to prepare a release tar file, run the following make target.

```bash
make release
```

## run from container

the master branch of this repository is built on every commit. using this
command will start a local HTTP server on port 8080. _(you can substitute `docker`
for `podman`)_

```bash
podman run --rm -it -p 8080:8080 quay.io/elmiko/social-git-snarfer
```
