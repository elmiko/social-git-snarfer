(function () {
  const msInOneDay = 24 * 60 * 60 * 1000;
  const msInThirtyDays = 30 * msInOneDay;
  const activityRectSize = 12;
  const GITHUB = "github";
  const GITLAB = "gitlab";
  const basegreen = "#c0f2b3";

  // create a new color string based on a count of event, making the color
  // darker with higher count.
  function newEventColorString(count) {
    const baseRed = 192;
    const baseGreen = 242;
    const baseBlue = 179;
    const delta = -5 * count;
    const gdelta = -4 * count;

    let red = Math.max(Math.min(baseRed + delta, 255), 0);
    let green = Math.max(Math.min(baseGreen + gdelta, 255), 0);
    let blue = Math.max(Math.min(baseBlue + delta, 255), 0);

    return `rgb(${red}, ${green}, ${blue})`;
  }

  function newEventObject(event) {
    let created_at = new Date(event.create_at);
    let source_host = event.source_host;
    return {created_at, source_host}
  }

  function newUsernamesObject(github, gitlab) {
    return { github, gitlab};
  }

  // figure out if a string is a single name or two names separated by
  // a colon. the returned object contains two keys: github and gitlab,
  // containing the username for each platform.
  function getUsernames(nameString) {
    let names = nameString.split(":");
    if (names.length == 1) {
      return newUsernamesObject(names[0], names[0])
    }
    return newUsernamesObject(names[0], names[1])
  }

  // created a Date for thirty days ago from now.
  function thirtyDaysAgo() {
    let now = Date.now();
    let ret = new Date();
    ret.setTime(now - msInThirtyDays);
    return ret;
  }

  // convert a Date into "YYYY-MM-DD"
  let getDateString = (date) => { return date.toJSON().slice(0, 10) };

  // return a Map with the keys being the last thirty days in YYYY-MM-DD
  // format, and the values being 0.
  function createEventCountMap() {
    let ecmap = new Map();
    let now = Date.now();
    let i = 0;
    do {
      let d = new Date();
      d.setTime(now - (i * msInOneDay));
      ecmap.set(getDateString(d), []);
      i += 1;
    } while (i < 30);
    return ecmap;
  }

  // return a new svg rect element to display activity density
  function createActivityRect(x, date, events) {
    let newRect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    newRect.setAttribute("x", x + 1);
    newRect.setAttribute("y", activityRectSize + 1);
    newRect.setAttribute("height", activityRectSize - 1);
    newRect.setAttribute("width", activityRectSize - 1);
    let count = events.length;
    if (count > 0) {
      // make the event rects darker with more activity
      newRect.setAttribute("fill", newEventColorString(count))
    }
    newRect.onmouseenter = function (event) {
      newRect.setAttribute("stroke", "#dddddd");
      displayActivityEntry(date, events);
    };
    newRect.onmouseleave = function (event) {
      newRect.setAttribute("stroke", "");
      displayActivityEntry();
    };
    return newRect;
  }

  // return a new svg text element to display activity date text
  function createActivityText(x, text) {
    let newText = document.createElementNS("http://www.w3.org/2000/svg", "text");
    newText.setAttribute("x", x + 3);
    newText.setAttribute("y", activityRectSize - 1);
    newText.append(text);
    return newText;
  }

  // display an individual event activity entry
  function displayActivityEntry(date, events) {
    const div = document.getElementById("div-activity");
    if (date == null && events == null) {
      div.innerHTML = "";
      return;
    }
    let count = events.length;
    let ghcount = 0;
    let glcount = 0;
    events.forEach(function(event) {
      switch (event.source_host) {
        case GITHUB:
          ghcount++;
          break;
        case GITLAB:
          glcount++;
          break;
        default:
          console.log("unknown event type found");
      }
    });
    let header = document.createElement("h2");
    header.innerText = count + " events found on " + date;
    div.appendChild(header);
    let ul = document.createElement("ul");
    let ghli = document.createElement("li");
    ghli.innerText = ghcount + " Github events";
    ul.appendChild(ghli);
    let glli = document.createElement("li");
    glli.innerText = glcount + " Gitlab events";
    ul.appendChild(glli);
    div.appendChild(ul);
  }

  // display an error message
  function displayError(message) {
    document.getElementById("h1-output").innerHTML = message;
  }

  // display the event history graphic
  function displayEvents(events, username) {
    const svg = document.getElementById("svg-output");
    let idx = 0;
    events.forEach(function (value, key, map) {
      let x = idx * activityRectSize;
      let text = key.slice(-2);
      let activityText = createActivityText(x, text);
      svg.appendChild(activityText);
      let activityRect = createActivityRect(x, key, value);
      svg.appendChild(activityRect);
      idx++;
    });
    document.getElementById("h1-output").innerHTML = "results for " + username;
  }

  function fetchEvents(url) {
    return fetch(url)
      .then(function (response) {
        if (!response.ok) {
          return null;
        }
        return response.json();
      })
      .catch(console.error);
  }

  function countEvents(rawEvents, eventsMap, eventSource) {
      rawEvents.forEach(function (item, index, array) {
        let created = getDateString(new Date(item["created_at"]));
        if (eventsMap.has(created)) {
          item.source_host = eventSource;
          let eventArr = eventsMap.get(created);
          eventArr.push(newEventObject(item));
          eventsMap.set(created, eventArr);
        }
      });
      return eventsMap;
  }

  let formUsername = document.getElementById("form-username");
  formUsername.onsubmit = function (event) {
    event.preventDefault();

    const svg = document.getElementById("svg-output");
    const children = Array.from(svg.childNodes);
    children.forEach(function (child) { child.remove(); });
    const h1 = document.getElementById("h1-output")
    h1.innerHTML = "Loading";
    const loader = window.setInterval(function () {
      if (h1.innerHTML.endsWith(".....")) { h1.innerHTML = "Loading"; }
      h1.innerHTML = h1.innerHTML + ".";
    }, 250);

    let inputUsername = document.getElementById("username");
    let usernames = getUsernames(inputUsername.value);
    var events = createEventCountMap();
    let githubEvents = fetchEvents("https://api.github.com/users/" + usernames.github + "/events?per_page=100");
    let gitlabEvents = fetchEvents("https://gitlab.com/api/v4/users/" + usernames.gitlab + "/events?per_page=100");
    Promise.all([githubEvents, gitlabEvents]).then(function (values) {
      window.clearInterval(loader);
      if (values[0] == null || values[1] == null) {
        let msg = "cannot find events for " + inputUsername.value;
        displayError(msg);
      } else {
        events = countEvents(values[0], events, GITHUB);
        events = countEvents(values[1], events, GITLAB);
        displayEvents(events, inputUsername.value);
      }
      inputUsername.value = "";
    });
  }

  function setupModal(modal, link) {
    link.onclick = function (event) {
      event.preventDefault();
      modal.style.display = "block";
    }
    modal.onclick = function (event) {
      modal.style.display = "none";
    }
  }
  setupModal(document.getElementById("help-modal"),
             document.getElementById("help-link"));
  setupModal(document.getElementById("license-modal"),
             document.getElementById("license-link"));

  var textHeader = document.getElementById("text-header");
  textHeader.style.marginBotton = "1em";
})();
