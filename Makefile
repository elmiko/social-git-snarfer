.PHONY: clean release


all: clean index.html

index.html:
	python3 build.py

clean:
	rm -f index.html social-git-snarfer.tar.bz2 social-git-snarfer.tar.bz2.sha512

release: clean index.html
	tar cjvf social-git-snarfer.tar.bz2 index.html LICENSE
	sha512sum social-git-snarfer.tar.bz2 > social-git-snarfer.tar.bz2.sha512

image : dockercmd ?= $(shell which docker || which podman || echo "docker")
image: clean index.html
	$(dockercmd) build -f ./hack/Dockerfile -t localhost/social-git-snarfer .
